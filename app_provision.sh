#!/bin/bash

sudo apt-get update
sudo apt-get install -y openjdk-8-jdk maven git tmux


git clone git@gitlab.com:dan-it/groups/devops3.git /home/ubuntu/


cd /home/ubuntu/devops3/StepProjects

./mvnw clean package


cp target/spring-petclinic-2.3.1.BUILD-SNAPSHOT.jar /home/ubuntu


export DB_HOST="3.81.105.39"
export DB_PORT="3306"
export DB_NAME="DB_NAME"
export DB_USER="DB_USER"
export DB_PASS="DB_PASS"

tmux new-session -d -s petclinic 'java -jar target/spring-petclinic-2.3.1.BUILD-SNAPSHOT.jar' 
